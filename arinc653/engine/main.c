#include <engine.h>

int main(void)
{
	Engine__main_mem main_mem;
	Engine__main_reset(&main_mem);
	while(1) {
		Engine__main_step(&main_mem);
	}

}
