/* --- Generated the 27/4/2017 at 12:18 --- */
/* --- heptagon compiler, version 1.00.06 (compiled tue. apr. 18 10:22:26 CET 2017) --- */
/* --- Command line: heptc -target c externc.epi --- */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "externc.h"

void Externc__read_bool_step(int*addr,int*value)
{
	printf("read bool(%x)", *addr);
	scanf("%d", value);
	usleep(500000);
}

void Externc__f1_step(int*i,int*o)
{
	*o =*i+5;
	usleep(500000);
	printf("f1(%d) returns (%d)\n", *i, *o);
}


void Externc__f2_step(int*i,int*o)
{
	*o =*i+2;
	usleep(500000);
	printf("f1(%d) returns (%d)\n", *i, *o);
}

void Externc__g_step(int*out)
{
	*out = 42;
	usleep(500000);
	printf("g() returns (%d)\n", *out);
}

void Externc__act_step(int*i)
{
	usleep(500000);
	printf("acting with (%d)\n", *i);
}
