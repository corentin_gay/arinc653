#===============================================================
# APPNAME
#===============================================================
# Heptagon part of the application
APPNAME     = simple
# Needed because I don't know how  to capitalize
APPNAME_CAP = Simple
# C part of the application
EXTERNC     = externc

#===============================================================
# FOLDERS
#===============================================================
# Heptagon library C files location
LIBDIR    = /home/lopht/lib/heptagon/c
# Where the generated C code goes
CGENDIR   = ./$(APPNAME)_c
# Where the external C code is expected to be
EXTERNDIR = ./$(EXTERNC)_c

#===============================================================
# BUILD ENVIRONMENT
#===============================================================
# Headers can be everywhere
GCC     = gcc -I$(LIBDIR) -I$(CGENDIR) -I$(EXTERNDIR)
# Only for compiling main.c, I need some special 
# definitions to avoid rewriting the file every time.
MAIN_CFLAGS = -DHEADER="<"$(APPNAME)".h>" \
		-DTHEADER="<"$(APPNAME)"_types.h>" \
		-DMODNAME=$(APPNAME_CAP)
# gcc is better at loading than ld itself (it knows
# where to find the crt0 files, a.s.o.
LD      = gcc

#===============================================================
# FILES
#===============================================================
EPTSRC      = ./$(APPNAME).ept
GEN_C       = $(CGENDIR)/$(APPNAME).c
GEN_C_TYPES = $(CGENDIR)/$(APPNAME)_types.c
GEN_H       = $(CGENDIR)/$(APPNAME).h
GEN_H_TYPES = $(CGENDIR)/$(APPNAME)_types.h
CMAIN       = ./main.c
CHEADERS    = $(LIBDIR)/pervasives.h $(EXTERNDIR)/$(EXTERNC).h $(GEN_H) $(GEN_H_TYPES) 
CFILES      = $(EXTERNDIR)/$(EXTERNC).c $(CMAIN) $(GEN_C) $(GEN_C_TYPES) 
OBJECTS     = $(CFILES:.c=.o)

#===============================================================
# LIBRARIES, which have a general compilation script
#===============================================================

all: $(APPNAME)

# Rule for application files
$(CGENDIR)/%.o : $(CGENDIR)/%.c $(CHEADERS)
	$(GCC) -c $< -o $@

# Rule for external files, which do not depend upon some headers
$(EXTERNDIR)/%.o : $(EXTERNDIR)/%.c $(LIBDIR)/pervasives.h
	$(GCC) -c $< -o $@

# Rule for main.c
main.o: main.c $(CHEADERS)
	$(GCC) $(MAIN_CFLAGS) -c $< -o $@

$(GEN_C) $(GEN_C_TYPES) $(GEN_H) $(GEN_H_TYPES) : $(EPTSRC) $(EXTERNC).epci
	heptc -target c $<

$(EXTERNC).epci: $(EXTERNC).epi
	heptc -c $<

$(APPNAME): $(OBJECTS)
	$(LD) $^ -o $@

clean:
	echo $(OBJECTS)
	rm -rf $(OBJECTS) $(APPNAME) $(CGENDIR) $(APPNAME).epci $(APPNAME).mls *~ $(EXTERNC).epci
