/* --- Generated the 27/4/2017 at 12:18 --- */
/* --- heptagon compiler, version 1.00.06 (compiled tue. apr. 18 10:22:26 CET 2017) --- */
/* --- Command line: heptc -target c externc.epi --- */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "externc.h"

void Externc__f_step(int*i,int*o)
{
	*o =*i+1;
	usleep(500000);
	//printf("f(%d) returns (%d)\n", *i, *o);
}

void Externc__g_step(int*i,int*o)
{
	*o =*i+5;
	usleep(500000);
	//printf("g(%d) returns (%d)\n", *i, *o);
}

void Externc__printx_step(int*i)
{
	printf("X : %d\n", *i);
}

void Externc__printy_step(int*i)
{
	printf("Y : %d\n", *i);
}


