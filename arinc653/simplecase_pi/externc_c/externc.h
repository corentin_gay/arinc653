/* --- Generated the 27/4/2017 at 12:18 --- */
/* --- heptagon compiler, version 1.00.06 (compiled tue. apr. 18 10:22:26 CET 2017) --- */
/* --- Command line: heptc -target c externc.epi --- */

#ifndef EXTERNC_H
#define EXTERNC_H

#include "stdbool.h"
#include "assert.h"
#include "pervasives.h"

void Externc__f_step(int*i,int*o);
void Externc__g_step(int*i,int*o);
void Externc__printx_step(int*i);
void Externc__printy_step(int*i);

#endif// EXTERNC_H
