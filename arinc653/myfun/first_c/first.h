/* --- Generated the 20/4/2017 at 11:26 --- */
/* --- heptagon compiler, version 1.00.06 (compiled tue. apr. 18 10:22:26 CET 2017) --- */
/* --- Command line: heptc -target c ./first.ept --- */

#ifndef FIRST_H
#define FIRST_H

#include "first_types.h"
void First__myfun_step(int* x, int* y, int* z);

typedef struct First__rcounter_mem {
  int v_1;
} First__rcounter_mem;

void First__rcounter_reset(First__rcounter_mem* self);

void First__rcounter_step(int* rst, int* cnt, First__rcounter_mem* self);

typedef struct First__counter_mem {
  int mem_cnt;
} First__counter_mem;

void First__counter_reset(First__counter_mem* self);

void First__counter_step(int* cnt, First__counter_mem* self);

#endif // FIRST_H
