/* --- Generated the 20/4/2017 at 11:26 --- */
/* --- heptagon compiler, version 1.00.06 (compiled tue. apr. 18 10:22:26 CET 2017) --- */
/* --- Command line: heptc -target c ./first.ept --- */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "first.h"

void First__myfun_step(int* x, int* y, int* z) {
  *z = ((*x)+(*y));
}

void First__rcounter_reset(First__rcounter_mem* self) {
  self->v_1 = 0;
}

void First__rcounter_step(int* rst, int* cnt, First__rcounter_mem* self) {
  
  int v;
  if ((*rst)) {
    *cnt = 0;
  } else {
    *cnt = self->v_1;
  };
  v = ((*cnt)+1);
  self->v_1 = v;;
}

void First__counter_reset(First__counter_mem* self) {
  self->mem_cnt = 0;
}

void First__counter_step(int* cnt, First__counter_mem* self) {
  
  int v;
  *cnt = self->mem_cnt;
  v = ((*cnt)+1);
  self->mem_cnt = v;;
}

