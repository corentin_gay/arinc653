#include <first.h>
#include <stdio.h>
#include <unistd.h>

int main(void)
{
/*
	int x,y,z;
	for (;;) {
		printf("Inputs:"); scanf("%d%d", &x, &y);
		First__myfun_step(&x,&y,&z);
		printf("Result: %d\n", z);
	}
*/
	char string[4];
	int cnt,rst, t;
	rst = 0;
	First__rcounter_mem mem;
	First__rcounter_reset(&mem);
	for(;;){
		rst = 0;
		printf("Do you want to reset the counter ?\n");
		t = scanf("%3s", string);
		if (!strcmp(string, "yes"))
			rst = 1;
		First__rcounter_step(&rst, &cnt, &mem);
		printf("Counter: %d\n", cnt);
	}
/*
	int cnt;
	First__counter_mem mem;
	First__counter_reset(&mem);
	for(;;){
		usleep(500000);
		First__counter_step(&cnt, &mem);
		printf("Counter: %d\n", cnt);
	}
*/

}
