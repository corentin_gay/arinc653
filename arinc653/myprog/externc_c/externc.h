/* --- Generated the 3/5/2017 at 16:38 --- */
/* --- heptagon compiler, version 1.00.06 (compiled tue. apr. 18 10:22:26 CET 2017) --- */
/* --- Command line: heptc -target c externc.epi --- */

#ifndef EXTERNC_H
#define EXTERNC_H

#include "stdbool.h"
#include "assert.h"
#include "pervasives.h"

void Externc__myread_step(int* size,float*samples) ;
void Externc__mywrite_step(int* size,float*samples) ;

#endif // EXTERNC_H
