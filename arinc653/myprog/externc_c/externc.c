/* --- Generated the 3/5/2017 at 16:38 --- */
/* --- heptagon compiler, version 1.00.06 (compiled tue. apr. 18 10:22:26 CET 2017) --- */
/* --- Command line: heptc -target c externc.epi --- */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "externc.h"

// Structure of a sample for machines with 2 channels,
// signed 16-bit audio capture.
struct Externc__sample {
  int16_t l ;
  int16_t r ;
} ;

// The input/output buffer used for exchanging data
#define MAXBUFSIZE 2048
static struct Externc__sample buf[MAXBUFSIZE] ;

// Samples communicated to/from the application are single-channel and
// of float type, to facilitate integration.
void Externc__myread_step(int* size,float*samples) {
  // Check size fit
  assert(*size<=MAXBUFSIZE) ;
  // Reading the data from standard input
  int r ;
  int r_acc = sizeof(struct Externc__sample)*(*size);
  char* tmp_buf = (char*)buf ;
  for(;r_acc;r_acc-=r,tmp_buf+=r) {
    r = read(0,tmp_buf,r_acc) ;
    if(r<0) {
      perror("read_samples error:") ;
      exit(0) ;
    }
  }
  // Convert to SAMPLE_TYPE, and extract only one channel.
  for(r=0;r<*size;r++) {
    samples[r] = (float)(buf[r].l) ;
  }
}

void Externc__mywrite_step(int* size,float*samples) {
  // Check size fit 
  assert(*size<=MAXBUFSIZE) ;
  // Convert to struct sample, and duplicate the input data
  // on the two channels
  int w ;
  for(w=0;w<*size;w++) {
    buf[w].l = (int32_t) samples[w] ;
    buf[w].r = buf[w].l ;
  }
  // Write the data
  char* tmp_buf = (char*)buf ;
  int w_acc = sizeof(struct Externc__sample)*(*size);
  for(;w_acc;w_acc-=w,tmp_buf+=w) {
    w = write(1,tmp_buf,w_acc) ;
    if(w<0) {
      perror("write_samples error:") ;
      exit(0) ;
    }
  }
  fflush(stdout) ;
}
