/* --- Generated the 9/5/2017 at 20:46 --- */
/* --- heptagon compiler, version 1.00.06 (compiled tue. apr. 18 10:22:26 CET 2017) --- */
/* --- Command line: heptc -target c myprog.ept --- */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "myprog.h"

void Myprog__sound_divided_step(float* f, float* o) {
  *o = ((*f)/2.000000);
}

void Myprog__read_write_sound_step() {
  
  int size;
  float vect[256];
  float output[256];
  size = 256;
  Externc__myread_step(&size, vect);
  {
    int i;
    for (i = 0; i < 256; ++i) {
      Myprog__sound_divided_step(&vect[i], &output[i]);
    }
  };
  Externc__mywrite_step(&size, output);;
}

void Myprog__main_step() {
  Myprog__read_write_sound_step();
}

