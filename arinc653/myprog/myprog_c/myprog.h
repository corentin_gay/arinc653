/* --- Generated the 9/5/2017 at 20:46 --- */
/* --- heptagon compiler, version 1.00.06 (compiled tue. apr. 18 10:22:26 CET 2017) --- */
/* --- Command line: heptc -target c myprog.ept --- */

#ifndef MYPROG_H
#define MYPROG_H

#include "myprog_types.h"
#include "externc.h"
void Myprog__sound_divided_step(float* f, float* o);

void Myprog__read_write_sound_step();

void Myprog__main_step();

#endif // MYPROG_H
