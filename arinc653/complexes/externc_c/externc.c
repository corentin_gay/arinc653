/* --- Generated the 3/5/2017 at 16:38 --- */
/* --- heptagon compiler, version 1.00.06 (compiled tue. apr. 18 10:22:26 CET 2017) --- */
/* --- Command line: heptc -target c externc.epi --- */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "externc.h"


void Externc__print_complex_step(Externc__complex* comp)
{
	printf("The vector is [%.3f + i%.3f]\n", comp->re, comp->im);
}
