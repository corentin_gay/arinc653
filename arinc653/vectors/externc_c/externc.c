/* --- Generated the 3/5/2017 at 16:38 --- */
/* --- heptagon compiler, version 1.00.06 (compiled tue. apr. 18 10:22:26 CET 2017) --- */
/* --- Command line: heptc -target c externc.epi --- */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "externc.h"

void Externc__complex_vector_create_step(float*f1,float*f2,float*f3,float*f4,float*f5,float*f6, Externc__complex* output)
{
	output[0].re = *f1;
	output[0].im = *f2;
	output[1].re = *f3;
	output[1].im = *f4;
	output[2].re = *f5;
	output[2].im = *f6;
	return output;
}

void Externc__print_vector_step(Externc__complex* vec)
{
	printf("The vector is [%.3f + %.3fi, %.3f + %.3fi, %.3f + %.3fi]\n",
	       vec[0].re, vec[0].im, vec[1].re, vec[1].im, vec[2].re, vec[2].im);
}
