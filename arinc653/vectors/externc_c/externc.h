/* --- Generated the 3/5/2017 at 16:38 --- */
/* --- heptagon compiler, version 1.00.06 (compiled tue. apr. 18 10:22:26 CET 2017) --- */
/* --- Command line: heptc -target c externc.epi --- */

#ifndef EXTERNC_H
#define EXTERNC_H

#include "stdbool.h"
#include "assert.h"
#include "pervasives.h"
typedef struct Externc__complex {
  float re;
  float im;
} Externc__complex;

void Externc__complex_vector_create_step(float*f1,float*f2,float*f3,float*f4,float*f5,float*f6,Externc__complex*output);
void Externc__print_vector_step(Externc__complex*vec);

#endif // EXTERNC_H
